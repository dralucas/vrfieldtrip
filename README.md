VR Field Trip
=============



 # Outreach - exodunes360 application
   ![ExoDunes360](http://exodunes360.fr/medias/resources/header_logo.png)
   dedicated website : http://exodunes360.fr
   
 # VRTitan - Full immersion in Titan's environment using VR
   
 # Field Work - Virtual field trip with teacher and student modes for exploring 3D terrain 
   ![VR Field trip on Mars](vrfieltrip.png)
   dedicated website : http://www.ipgp.fr/~lucas/VRTerrain/
   
 # Sapine 3D - potree exploration of Gauge station at Sapine river outlet
   ![3D cloud at Sapine Gauge](sapine3d.png)